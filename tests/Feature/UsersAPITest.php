<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UsersAPITest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_basic_request()
    {
        $response = $this->get('/api/v1/users');

        $response->assertStatus(200);
    }
    /**
     * Making An Api Request.
     *
     * @return void
     */
    public function test_making_an_api_request()
    {
        $response = $this->get('/api/v1/users');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', 6)
            );
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function test_api_request_with_provider_filter()
    {
        $response = $this->get('/api/v1/users?provider=DataProviderY');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', 3)
                    ->has('data.0', fn ($json) =>
                        $json->where('id', "4fc2-a8d1")
                             ->where('balance', 300)
                             ->where('currency', 'AED')
                             ->where('email', 'parent7@parent.eu')
                             ->where('status', 'authorised')
                             ->where('created_at', '2018-12-22')
                    )
            );
    }


    /**
     * api request with balance filter.
     *
     * @return void
     */
    public function test_api_request_with_balance_filter()
    {
        $response = $this->get('/api/v1/users?balanceMin=10&balanceMax=100');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', 3)
                    ->has('data.0', fn ($json) =>
                        $json->where('id', "d3d29d70-4334-11e3-34-034165a3a613")
                             ->where('balance', 10)
                             ->where('currency', 'USD')
                             ->where('email', 'parent2@parent.eu')
                             ->where('status', 'decline')
                             ->where('created_at', '2018-11-30')
                    )
            );
    }


    /**
     * api request with status filter.
     *
     * @return void
     */
    public function test_api_request_with_status_filter()
    {
        $response = $this->get('/api/v1/users?statusCode=authorised');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', 2)
                    ->has('data.0', fn ($json) =>
                        $json->where('id', "d3d29d70-1d25-11e3-8591-343lkj3")
                             ->where('balance', 200)
                             ->where('currency', 'AED')
                             ->where('email', 'parent1@parent.eu')
                             ->where('status', 'authorised')
                             ->where('created_at', '2018-11-30')
                    )
            );
    }

    /**
     * api request with currency filter.
     *
     * @return void
     */
    public function test_api_request_with_currency_filter()
    {
        $response = $this->get('/api/v1/users?currency=USD');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', 3)
                    ->has('data.0', fn ($json) =>
                        $json->where('id', "d3d29d70-4334-11e3-34-034165a3a613")
                             ->where('balance', 10)
                             ->where('currency', 'USD')
                             ->where('email', 'parent2@parent.eu')
                             ->where('status', 'decline')
                             ->where('created_at', '2018-11-30')
                    )
            );
    }

    /**
     * all filter together.
     *
     * @return void
     */
    public function test_all_filter_together()
    {
        $response = $this->get('/api/v1/users?currency=USD&statusCode=decline&balanceMin=10&balanceMax=100&provider=DataProviderX');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('data', 1)
                    ->has('data.0', fn ($json) =>
                        $json->where('id', "d3d29d70-4334-11e3-34-034165a3a613")
                             ->where('balance', 10)
                             ->where('currency', 'USD')
                             ->where('email', 'parent2@parent.eu')
                             ->where('status', 'decline')
                             ->where('created_at', '2018-11-30')
                    )
            );
    }
}
