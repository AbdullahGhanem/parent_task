<?php

namespace App\Http\Controllers;

use App\DataProviders\DataProviderX;
use App\DataProviders\DataProviderY;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {	
    	$status = $request->statusCode;
    	$min = $request->balanceMin;
    	$max = $request->balanceMax;
    	$currency = $request->currency;

    	switch ($request->provider) {
    		case 'DataProviderY':
    			$data_y = new DataProviderY();
    			$users = $data_y->getData();
    			break;    		
    		case 'DataProviderX':
    			$data_x = new DataProviderX();
    			$users = $data_x->getData();
    			break;
    		
    		default:
    		    $data_y = new DataProviderY();
    			$users_y = $data_y->getData();

    			$data_x = new DataProviderX();
    			$users_x = $data_x->getData();

    			$users = $users_x->merge($users_y);
    			break;
    	}

    	$users = $users->when($status, function($query) use($status) {
    		return $query->where('status', $status);
    	})
    	->when($min && $max, function($query) use($min, $max) {
    		return $query->whereBetween('balance', [$min, $max]);
    	})
    	->when($currency, function($query) use($currency) {
    		return $query->where('currency', $currency);
    	});

        return UserResource::collection($users);
    }
}
