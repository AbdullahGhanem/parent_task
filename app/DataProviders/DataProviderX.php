<?php

namespace App\DataProviders;

use App\DataProviders\DataProvider;
use Carbon\Carbon;

class DataProviderX extends DataProvider
{
    /**
     * The name of the "JSON FILE NAME".
     *
     * @var string
     */
    const JSON_FILE_NAME = 'DataProviderX.json';

    /**
     * The name of the "lookups".
     *
     * @var array
     */
    protected $lookups = [
        'id' => 'parentIdentification',
        'balance' => 'parentAmount',
        'currency' => 'Currency',
        'email' => 'parentEmail',
        'status' => 'statusCode',
        'created_at' => 'registerationDate',
    ];

    /**
     * The name of the "status lookups".
     *
     * @var array
     */
    protected $status_lookups = [
        '1' => 'authorised',
        '2' => 'decline',
        '3' => 'refunded',
    ];
    
    /**
     * get Lookups.
     *
     * @return Array
     */
    public function getLookups(): Array
    {
        return $this->lookups;
    }

    /**
     * get Status Lookups.
     *
     * @return Array
     */
    public function getStatusLookups(): Array
    {
        return $this->status_lookups;
    }

    /**
     * formate Created At Date.
     *
     * @param  String  $date
     * @return \Carbon\Carbon
     */
    public function formateDate($date): Carbon
    {
        return Carbon::parse($date);
    }
}