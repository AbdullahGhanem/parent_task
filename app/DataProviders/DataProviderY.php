<?php

namespace App\DataProviders;

use App\DataProviders\DataProvider;
use Carbon\Carbon;

class DataProviderY extends DataProvider
{
    /**
     * The name of the "JSON FILE NAME".
     *
     * @var string
     */
    const JSON_FILE_NAME = 'DataProviderY.json';

    /**
     * The name of the "lookups".
     *
     * @var array
     */
    protected $lookups = [
        'id' => 'id',
        'amount' => 'amount',
        'currency' => 'currency',
        'email' => 'email',
        'status' => 'status',
        'created_at' => 'created_at',
    ];

    /**
     * The name of the "status lookups".
     *
     * @var array
     */
    protected $status_lookups = [
        '100' => 'authorised',
        '200' => 'decline',
        '300' => 'refunded',
    ];

    
    /**
     * get Lookups.
     *
     * @return Array
     */
    public function getLookups(): Array
    {
        return $this->lookups;
    }

    
    /**
     * get Status Lookups.
     *
     * @return Array
     */
    public function getStatusLookups(): Array
    {
        return $this->status_lookups;
    }
    
    /**
     * formate Created At Date.
     *
     * @param  String  $date
     * @return \Carbon\Carbon
     */
    public function formateDate($date): Carbon
    {
        return Carbon::createFromFormat('d/m/Y', $date);
    }

}