<?php

namespace App\DataProviders;

use Carbon\Carbon;


abstract class DataProvider
{
    /**
     * json file name.
     *
     * @var String
     */
	public $json_file_name;

    /**
     * Create a new Data Provider.
     *
     * @return void
     */
    final public function __construct()
    {
        $this->json_file_name = static::JSON_FILE_NAME;
    }

    /**
     * formate Date.
     * 
     * @param  string  $date
     * @return Carbon/Carbon
     */
	abstract public function formateDate($date): Carbon;

    /**
     * get Lookups.
     *
     * @return Array
     */
	abstract public function getLookups(): Array;

    /**
     * get Status Lookups.
     *
     * @return Array
     */
	abstract public function getStatusLookups(): Array;


    /**
     * get Data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getData()
    {
        $file = file_get_contents( public_path('jsons/'. $this->json_file_name));
        $collection = collect(json_decode($file, true)['data']);

        $users = $collection->map(function ($user) {
            return [
                'id' => $user[$this->getLookups()['id'] ?? 'id'],
                'balance' => (int) $user[$this->getLookups()['balance'] ?? 'balance'],
                'currency' => $user[$this->getLookups()['currency'] ?? 'currency'],
                'email' => $user[$this->getLookups()['email'] ?? 'email'],
                'status' => $this->getStatusLookups()[ $user[$this->getLookups()['status'] ?? 'status'] ],
                'created_at' => $this->formateDate($user[$this->getLookups()['created_at'] ?? 'created_at']),
            ];
        });
        return $users;
    }
}
